import { NgModule } from "@angular/core";
import { Routes } from "@angular/router";
import { NativeScriptRouterModule } from "nativescript-angular/router";
import { ListComponent } from "./list.component";
import { ListDetalleComponent } from "./detalle/list-detalle.component";


const routes: Routes = [
    { path: "", component: ListComponent },
    { path: "detalle", component: ListDetalleComponent}
];

@NgModule({
    imports: [NativeScriptRouterModule.forChild(routes)],
    exports: [NativeScriptRouterModule]
})
export class NavigateRoutingModule { }
