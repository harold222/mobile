import { Component, OnInit } from "@angular/core";
import { RadSideDrawer } from "nativescript-ui-sidedrawer";
import * as app from "tns-core-modules/application";
import { Store } from "@ngrx/store";
import { AppState } from "../app.module";
import * as Toast from "nativescript-toast";

@Component({
    selector: "Home",
    templateUrl: "./home.component.html"
})
export class HomeComponent implements OnInit {
    favs: Array<string>;
    constructor(private store: Store<AppState>) {
        // Use the component constructor to inject providers.
        this.favs = [];
    }

    ngOnInit(): void {
        this.store
            .select(state => state.favoritos.items)
            .subscribe(data => {
                data && Toast.makeText("Agregado a leer ahora").show();
                this.favs = data.map(m=> m.nombre);
            });
    }

    onDrawerButtonTap(): void {
        const sideDrawer = <RadSideDrawer>app.getRootView();
        sideDrawer.showDrawer();
    }
}
